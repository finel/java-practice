package com.rc.web.controller;

import com.rc.component.annotation.RAutowired;
import com.rc.component.annotation.RController;
import com.rc.component.annotation.RRequestMapping;
import com.rc.web.service.TestService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author: ranchao
 * @date: 2019/8/29/029 11:35
 */
@RController
@RRequestMapping("/test")
public class TestController {

    @RAutowired
    private TestService testService;

    @RRequestMapping("/hi")
    public String sayHi(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("name");
        String res = this.testService.sayHello(name);
        try {
            response.getWriter().write(res);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

}
