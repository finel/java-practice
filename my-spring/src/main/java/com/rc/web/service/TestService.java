package com.rc.web.service;

/**
 * @author: ranchao
 * @date: 2019/8/29/029 11:35
 */
public interface TestService {

    /**
     * say hello
     * @param name 名字
     * @return 打招呼语句
     */
    String sayHello(String name);
}
