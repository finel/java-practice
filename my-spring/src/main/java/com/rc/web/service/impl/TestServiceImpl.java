package com.rc.web.service.impl;

import com.rc.component.annotation.RService;
import com.rc.web.service.TestService;

/**
 * @author: ranchao
 * @date: 2019/8/29/029 11:35
 */
@RService("testService")
public class TestServiceImpl implements TestService {

    @Override
    public String sayHello(String name) {
        return "Hi! " + name;
    }
}
