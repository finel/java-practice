package com.rc.component.servlet;

import com.rc.component.annotation.RAutowired;
import com.rc.component.annotation.RController;
import com.rc.component.annotation.RRequestMapping;
import com.rc.component.annotation.RService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;

/**
 * @author: ranchao
 * @date: 2019/8/26/026 18:21
 */
public class RDispatchServlet extends HttpServlet {


    private Properties contextConfigs = new Properties();

    private List<String> classNameList = new ArrayList<String>();

    private Map<String, Object> iocMap = new HashMap<String, Object>();

    private Map<String, Method> handleMapping = new HashMap<String, Method>();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            doDispatch(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
            resp.getWriter().write("500 Exception Detail:\n" + Arrays.toString(e.getStackTrace()));
        }
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.doLoadConfig(config.getInitParameter("contextConfigLocation"));
        this.doScanner(this.contextConfigs.getProperty("scanner-package"));
        this.doInstance();
        this.doAutowried();
        this.doHandlerMapping();
        System.out.println("XSpring FrameWork is init.");
        this.doPrintData();
    }

    /**
     * 请求路径转发
     * 调用对应的方法
     * @param request
     * @param response
     * @throws IOException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    private void doDispatch(HttpServletRequest request, HttpServletResponse response) throws IOException, InvocationTargetException, IllegalAccessException {
        String url = request.getRequestURI();
        String contextPath = request.getContextPath();

        url = url.replaceAll(contextPath, "").replaceAll("/+", "/");

        System.out.println("[INFO-7] request url-->" + url);

        if (!this.handleMapping.containsKey(url)) {
            response.getWriter().write("404 NOT FOUND!!");
            return;
        }

        Method method = this.handleMapping.get(url);
        System.out.println("[INFO-7] method-->" + method);

        String beanName = this.toLowerFirstCase(method.getDeclaringClass().getSimpleName());
        System.out.println("[INFO-7] iocMap.get(beanName)->" + iocMap.get(beanName));

        method.invoke(iocMap.get(beanName), request, response);

        System.out.println("[INFO-7] method.invoke put {" + iocMap.get(beanName) + "}.");
    }

    private String toLowerFirstCase(String className) {
        char[] chars = className.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }

    /**
     * 加载配置文件
     * @param contextConfigLocation
     */
    private void doLoadConfig(String contextConfigLocation) {
        try (InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(contextConfigLocation)){
            contextConfigs.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 扫描包
     * @param scannerPackageLocation
     */
    private void doScanner(String scannerPackageLocation) {
        if (scannerPackageLocation == null || "".equals(scannerPackageLocation.trim())) {
            return;
        }
        String path = ("/" + scannerPackageLocation.replaceAll("\\.", "/"));
        System.out.println("[INFO-2] scanner package " + path + " .");
        URL resource = this.getClass().getClassLoader().getResource(path);
        if (resource == null) {
            return;
        }
        File classpath = new File(resource.getFile());

        for (File file : classpath.listFiles()) {
            String fileName = file.getName();
            if (file.isDirectory()) {
                System.out.println("[INFO-2] {" + fileName + "} is a directory.");
                // 递归扫描子目录
                doScanner(scannerPackageLocation + "." + fileName);
            } else {
                if (!fileName.endsWith(".class")) {
                    System.out.println("[INFO-2] {" + fileName + "} is not a class file.");
                    continue;
                }
                String className = scannerPackageLocation + "." + fileName.replaceAll(".class", "");
                classNameList.add(className);
                System.out.println("[INFO-2] {" + className + "} has been saved in classNameList.");
            }
        }

    }

    /**
     * 实例化对象
     */
    private void doInstance() {
        if (classNameList.isEmpty()) {
            return;
        }

        classNameList.forEach(className -> {
            try {
                Class<?> clazz = Class.forName(className);
                if (clazz.isAnnotationPresent(RController.class)) {
                    String beanName = this.toLowerFirstCase(clazz.getSimpleName());
                    Object instance = clazz.newInstance();
                    iocMap.put(beanName, instance);
                    System.out.println("[INFO-3] {" + beanName + "} has been saved in iocMap.");

                }

                if (clazz.isAnnotationPresent(RService.class)) {
                    String beanName = this.toLowerFirstCase(clazz.getSimpleName());
                    RService rService = clazz.getAnnotation(RService.class);
                    if (!"".equals(rService.value())) {
                        beanName = rService.value();
                    }
                    Object instance = clazz.newInstance();
                    iocMap.put(beanName, instance);
                    System.out.println("[INFO-3] {" + beanName + "} has been saved in iocMap.");

                    // 遍历接口
                    for (Class<?> i : clazz.getInterfaces()) {
                        beanName = toLowerFirstCase(i.getSimpleName());
                        if (iocMap.containsKey(beanName)) {
                            throw new Exception("The Bean Name Is Exist.");
                        }
                        iocMap.put(beanName, instance);
                        System.out.println("[INFO-3] {" + beanName + "} has been saved in iocMap.");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * 扫描自动注入
     */
    private void doAutowried() {
        if (iocMap.isEmpty()) {
            return;
        }
        iocMap.forEach((key, value) -> {
            Field[] fields = value.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (!field.isAnnotationPresent(RAutowired.class)) {
                    continue;
                }
                System.out.println("[INFO-4] Existence XAutowired.");

                RAutowired autowired = field.getAnnotation(RAutowired.class);
                String beanName = autowired.value().trim();

                if ("".equals(beanName)) {
                    Class<?> type = field.getType();
                    beanName = toLowerFirstCase(type.getSimpleName());
                }
                field.setAccessible(true);
                try {
                    Object instance = iocMap.get(beanName);
                    field.set(value, instance);
                    System.out.println("[INFO-4] field set {" + value + "} - {" + instance + "}.");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    /**
     * 扫描controller中请求路径对应的方法映射
     */
    private void doHandlerMapping() {
        if (iocMap.isEmpty()) {
            return;
        }

        iocMap.forEach((key, value) -> {
            Class<?> clazz = value.getClass();
            if (clazz.isAnnotationPresent(RController.class)) {
                String baseUrl = "";
                if (clazz.isAnnotationPresent(RRequestMapping.class)) {
                    RRequestMapping requestMapping = clazz.getAnnotation(RRequestMapping.class);
                    baseUrl = requestMapping.value();
                }
                Method[] methods = clazz.getDeclaredMethods();
                for (Method method : methods) {
                    if (!method.isAnnotationPresent(RRequestMapping.class)) {
                        continue;
                    }
                    RRequestMapping requestMapping = method.getAnnotation(RRequestMapping.class);
                    String path = requestMapping.value();
                    String url = "/" + baseUrl + "/" + path;
                    url = url.replaceAll("/+", "/");
                    handleMapping.put(url, method);
                    System.out.println("[INFO-5] handlerMapping put {" + url + "} - {" + method + "}.");
                }
            }
        });
    }


    private void doPrintData() {
        System.out.println("[INFO-6]----data------------------------");

        System.out.println("contextConfig.propertyNames()-->" + contextConfigs.propertyNames());
        System.out.println("[classNameList]-->");
        classNameList.forEach(System.out::println);
        System.out.println("[iocMap]-->");
        iocMap.entrySet().forEach(System.out::println);
        System.out.println("[handlerMapping]-->");
        handleMapping.entrySet().forEach(System.out::println);
        System.out.println("[INFO-6]----done-----------------------");

        System.out.println("====启动成功====");
        System.out.println("测试地址：http://localhost:8080/test/query?username=xiaopengwei");
        System.out.println("测试地址：http://localhost:8080/test/listClassName");
    }

}

