package com.rc.component.annotation;

import java.lang.annotation.*;

/**
 * @author: ranchao
 * @date: 2019/8/26/026 18:14
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RAutowired {

    String value() default "";

}
