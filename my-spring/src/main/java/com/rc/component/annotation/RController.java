package com.rc.component.annotation;

import java.lang.annotation.*;

/**
 * @author: ranchao
 * @date: 2019/8/26/026 18:13
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RController {

    String value() default "";

}
